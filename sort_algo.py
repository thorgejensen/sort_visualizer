from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QHBoxLayout, QSizePolicy, QPushButton, QSlider, QComboBox
from PyQt5.QtCore import QThread, Qt, pyqtSlot, pyqtSignal

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot

import sys
import random

import sort_instance

stdColour = 'r'

startRange = 0
endRange = 100

def randList(endRange, num, startRange=0):
    '''Get a list of random numbers in the range startRange .. endRange'''

    return [random.randint(startRange, endRange) for i in range(num)]

class Ui(QMainWindow):

    def __init__(self):
        super().__init__()
        uic.loadUi('sort_algo.ui', self)
        
        random.seed()
        self.thread = QThread()
        self.sortInstances = []
        
        btnSort = self.findChild(QPushButton, 'btnSort')
        btnSort.clicked.connect(self.btnSortClicked)
        
        btnCreate = self.findChild(QPushButton, 'btnCreate')
        btnCreate.clicked.connect(self.btnNewArrayClicked)
        
        horSlider = self.findChild(QSlider, 'horSlider')
        self.curNum = horSlider.value()
        self.curArray = randList(endRange, self.curNum)
        
        cbxSortAlgo = self.findChild(QComboBox, 'cbxSortAlgo')
        listSortAlgos = [self.tr('Bubble Sort'), self.tr('Merge Sort'), self.tr('Quick Sort'), self.tr('Heap Sort')]
        cbxSortAlgo.clear()
        cbxSortAlgo.addItems(listSortAlgos)
        
        self.canvas = PlotCanvas(self, width=5, height=4)
        self.verticalLayout.addWidget(self.canvas, 1)
        
        self.show()
        
    def btnNewArrayClicked(self):
        horSlider = self.findChild(QSlider, 'horSlider')
        self.curNum = horSlider.value()
        self.curArray = randList(endRange, self.curNum)
        
        # Simplify notation slightly
        canvas = self.canvas
        myAxes = self.canvas.figure.axes[0]
        
        # Make sure we only show one plot
        n = len(canvas.figure.axes)
        if n > 1:
            while n > 1:
                canvas.figure.delaxes(canvas.figure.axes[n-1])
                n -= 1
            
            myAxes.change_geometry(1, 1, 1)
            canvas.draw()
        
        # Refresh the canvas
        canvas.plot(0, self.curArray)
    
    def btnSortClicked(self):
        # Disable the buttons while we are applying an algorithm
        btnSort = self.findChild(QPushButton, 'btnSort')
        btnCreate = self.findChild(QPushButton, 'btnCreate')
        btnSort.setEnabled(False)
        btnCreate.setEnabled(False)
    
        cbxSortAlgo = self.findChild(QComboBox, 'cbxSortAlgo')  
        
        # Apply the chosen sort algorithm
        print("Applying ", cbxSortAlgo.currentText())

        # Create a new sort instance and connect its signals to the corresponding plotting routines
        sortInstance = sort_instance.sortInstance(self.curArray, cbxSortAlgo.currentIndex())
        sortInstance.plotDataReady.connect(self.canvas.plot, type=Qt.BlockingQueuedConnection)
        sortInstance.plotDataUpdated.connect(self.canvas.updatePlot, type=Qt.BlockingQueuedConnection)
        sortInstance.requireNumSubplots.connect(self.canvas.ensureNumSubplots, type=Qt.BlockingQueuedConnection)
        
        # Move the sort instance to the thread
        sortInstance.moveToThread(self.thread)
        
        # Connect the sort instance's finished signal to the thread's slot
        sortInstance.finished.connect(self.thread.quit)
        sortInstance.finished.connect(sortInstance.deleteLater)
        sortInstance.finished.connect(self.enableButtons)
        
        # Connect the thread started signal to the sort instance operational slot method
        self.thread.started.connect(sortInstance.sort)
        
        self.sortInstances.append(sortInstance)
        
        self.thread.start()

    pyqtSlot()
    def enableButtons(self):
        btnSort = self.findChild(QPushButton, 'btnSort')
        btnCreate = self.findChild(QPushButton, 'btnCreate')
        
        # Enable the buttons again
        btnSort.setEnabled(True)
        btnCreate.setEnabled(True)
        
        
class PlotCanvas(FigureCanvas):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                QSizePolicy.Expanding,
                QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        
        myAxes = self.figure.add_subplot(1,1,1)
        myAxes.set_yticks(range(startRange, endRange, (endRange - startRange)//5))
        myAxes.set_xticks(range(0, parent.curNum, 5))
        myAxes.set_title('Visualizing Array')
        myAxes.bar(range(parent.curNum), parent.curArray, 0.7, alpha=0.5, color='r')
        
        #self.myBackground = self.figure.canvas.copy_from_bbox(self.myAxes.bbox)
        
        self.draw()
    
    @pyqtSlot(int, list)
    def plot(self, axesIndex, data):
        curNum = len(data)
        axes = self.figure.axes[axesIndex]
        
        axes.set_yticks(range(startRange, endRange, (endRange - startRange)//5))
        axes.set_xticks(range(0, curNum, 5))
        
        axes.clear()
        axes.bar(range(curNum), data, 0.7, alpha=0.5, color=stdColour)
        self.draw()
    
    @pyqtSlot(int, list, list, list)
    def updatePlot(self, axesIndex, data, colours, alphaVals):
        curNum = len(data)
        
        axes = self.figure.axes[axesIndex]
        #axes.draw_artist(axes.patch)
        patches = axes.get_children()[0:curNum]
        for rect, h, colour, alpha in zip(patches, data, colours, alphaVals):
            # Redraw only the rectangles that have changed
            if (rect.get_facecolor() != matplotlib.colors.to_rgba(colour, alpha)) or (rect.get_height() != h):
                # First cover the existing patch with a white patch
                rect.set(color='w', linewidth=2, alpha=1.0)
                axes.draw_artist(rect)
            
                # Then redraw the patch we want
                if h:
                    rect.set_height(h)
                else:
                    rect.set_height(0)
                rect.set(color = colour, linewidth=1.0, alpha=alpha)
                axes.draw_artist(rect)
            
        self.figure.canvas.update()
        self.figure.canvas.flush_events()
    
    def pushSubplot(self):
        n = len(self.figure.axes)
        for index in range(n):
            self.figure.axes[index].change_geometry(n+1, 1, index + 1)

        # Add the new subplot
        ax = self.figure.add_subplot(n+1, 1, n+1)
        
        # Redraw to update
        self.draw()

    def popSubplot(self):
        n = len(self.figure.axes)
        # Remove the last subplot
        self.figure.delaxes(self.figure.axes[n-1])
        
        for index in range(n-1):
            self.figure.axes[index].change_geometry(n-1, 1, index+1)
        
        # Redraw to update
        self.draw()
    
    @pyqtSlot(int)
    def ensureNumSubplots(self, num):
        # Set up sufficiently many subplots
        while len(self.figure.axes) < num:
            self.pushSubplot()
            
if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = Ui()
    sys.exit(app.exec_())