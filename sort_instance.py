from PyQt5.QtCore import QThread, QObject, pyqtSignal, pyqtSlot

import time
import math
import random

stdColour = 'r'
focusColour = 'g'
fixedColour = 'b'

#sleepTime = 0.038
sleepTime = 0

class sortInstance(QObject):
    finished = pyqtSignal()
    plotDataReady = pyqtSignal(int, list)
    plotDataUpdated = pyqtSignal(int, list, list, list)
    requireNumSubplots = pyqtSignal(int)

    def __init__(self, curArray, sortAlgoIndex):
        ''' Initialize the sort instance and pass the array to sort '''
        QObject.__init__(self)
        self.curArray = curArray
        self.sortAlgoIndex = sortAlgoIndex

    def bubbleSort(self, plot=True):
        '''Apply bubble sort to the given array'''
        curArray = self.curArray
        
        # Get the length of the given array
        curNum = len(curArray)

        # Start timing
        if plot:
            tstart = time.time()
            frames = 0
            colours = [stdColour] * curNum
        
        n = curNum
        swapped = True
        while swapped:
            # The sequence is sorted if we have performed no swaps in the last iteration
            swapped = False
            
            # Bubble the element at position i to the right until we find a larger
            # element. In this case, continue with the larger element
            for i in range(n - 1):
                # Swap the element at position with the one at position i+1 if necessary
                if curArray[i] > curArray[i+1]:
                    curArray[i], curArray[i+1] = curArray[i+1], curArray[i]
                    
                    # Remember that we have swapped
                    swapped = True
                    
                if plot:
                    frames += 1
                    
                    if i == n - 2:
                        # Everything in positiones >= n-1 will not be touched any more
                        # These entries will be coloured accordingly
                        colours[n-1] = fixedColour
                        tempColours = colours.copy()
                    else:
                        # Otherwise keep track where the current focus lies
                        tempColours = colours.copy()
                        tempColours[i+1] = focusColour
                    self.plotDataUpdated.emit(0, curArray, tempColours, [0.5] * curNum)
                    time.sleep(sleepTime)
                    
            n -= 1

        if plot:
            timeLapsed = time.time() - tstart
            if timeLapsed > 0:
                print('FPS:', frames/timeLapsed)
            
    def merge(self, source, dest, startPoint, inc, plot=True):
        '''Merge source[startPoint:startPoint+inc] and source[startPoint+inc:startPoint+2*inc] into dest.'''
        # Initialize necessary variables
        curNum = len(source)
        
        endPoint1 = startPoint + inc
        endPoint2 = min(startPoint + 2*inc, curNum)
        sourceIndex1 = startPoint
        sourceIndex2 = startPoint + inc
        destIndex = startPoint
        
        if plot:
            frames = 0
        
        # Merge  source[startPoint:startPoint+inc] and source[startPoint+inc:endPoint2]
        while (sourceIndex1 < endPoint1) and (sourceIndex2 < endPoint2):
            # Copy the next element
            if source[sourceIndex1] < source[sourceIndex2]:
                dest[destIndex] = source[sourceIndex1]
                sourceIndex1 += 1
            else:
                dest[destIndex] = source[sourceIndex2]
                sourceIndex2 += 1
            
            if plot:
                frames += 1
                colours = [stdColour] * curNum
                colours[destIndex] = focusColour
                self.plotDataUpdated.emit(0, dest, colours, [0.5] * curNum)
                time.sleep(sleepTime)
            
            destIndex += 1
        
        # Copy the remaining entries
        if sourceIndex1 < endPoint1:
            dest[destIndex:endPoint2] = source[sourceIndex1:endPoint1]
        if sourceIndex2 < endPoint2:
            dest[destIndex:endPoint2] = source[sourceIndex2:endPoint2]
            
        if plot:
            frames += 1
            self.plotDataUpdated.emit(0, dest, [stdColour] * curNum, [0.5] * curNum)
            time.sleep(sleepTime)
        
        return frames
    
    def mergeSort(self, curArray, plot=True):
        '''Apply merge sort to the given array'''
        
        # Get the length of the array to sort
        curNum = len(curArray)
        
        # Start timing
        if plot:
            tstart = time.time()    
            frames = 0
            
            self.requireNumSubplots.emit(2)
            self.plotDataReady.emit(1, curArray)
            time.sleep(sleepTime)
            
        # Introduce necessary variables
        logCurNum = math.ceil(math.log(curNum, 2))
        source = curArray
        dest = [None] * curNum
        
        # The idea is to use axesIndex=1 to illustrate source
        # and axesIndex=0 to illustrate dest
        
        for inc in (2**k for k in range(logCurNum)):
            for startPoint in range(0, curNum, 2*inc):
                if plot:
                    frames += 1
                    # Use the alphaVals in myListPlot2 to focus on the two subarrays that we will merge next
                    alphaVals = [0.3 for i in range(startPoint)] + [0.7 for i in range(startPoint, startPoint + 2*inc)] + [0.3 for i in range(startPoint + 2*inc, curNum)]
                    self.plotDataUpdated.emit(1, source, [stdColour] * curNum, alphaVals)
                    time.sleep(sleepTime)
                frames += self.merge(source, dest, startPoint, inc)
            source, dest = dest, source

        if curArray is not source:
            curArray[0:curNum] = source[0:curNum]
        
        if plot:
            frames += 1
            self.plotDataUpdated.emit(0, curArray, [stdColour] * curNum, [0.5] * curNum)
            
            timeLapsed = time.time() - tstart
            if timeLapsed > 0:
                print('FPS:', frames/timeLapsed)
    
    def quickSort(self, curArray, plot=True):
        '''Apply in-place quick sort to the given array'''
        curNum = len(curArray)
        
        # Start timing
        if plot:
            tstart = time.time()    
            frames = 0
            
            colours = [stdColour] * curNum
        
        # Initially the stack contains the boundaries of the whole array
        stack = [(0, curNum-1)]
        
        while len(stack) > 0:
            # Next we will look at the subarray between the indices a and b
            a,b = stack.pop()

            # Randomly choose the pivot
            pivotIndex = random.randint(a, b)

            if plot:
                alphaVals = [0.3] * a + [0.7] * (b - a + 1) + [0.3] * (curNum - 1 - b)
                frames += 1
                tmpColours = colours.copy()
                tmpColours[pivotIndex] = 'y'                
                self.plotDataUpdated.emit(0, curArray, tmpColours, alphaVals)
                time.sleep(sleepTime)

            left = a
            right = b - 1
            
            # Swap the chosen pivot element with position b
            curArray[pivotIndex], curArray[b] = curArray[b], curArray[pivotIndex]
            pivot = curArray[b]
            
            if plot:
                frames += 1
                tmpColours = colours.copy()
                tmpColours[b] = 'y'
                self.plotDataUpdated.emit(0, curArray, tmpColours, alphaVals)
                time.sleep(sleepTime)
                
           
            while left <= right:
                # Scan right until we find an element >= than our pivot
                # Note that if all elements between position left and position right
                # are smaller than the pivot, we will end up with left = right + 1 = b
                while (left <= right) and (curArray[left] < pivot):
                    left += 1
                    
                    if plot:
                        frames += 1
                        tmpColours = colours.copy()
                        tmpColours[left] = focusColour
                        tmpColours[right] = focusColour
                        tmpColours[b] = 'y'
                        self.plotDataUpdated.emit(0, curArray, tmpColours, alphaVals)
                        time.sleep(sleepTime)
                
                # Scan left until we find an element <= than our pivot
                # Note that if all elements between position left and position right
                # are larger than the pivot, we will end up with right = left - 1
                while (left <= right) and (pivot < curArray[right]):
                    right -= 1
                   
                    if plot:
                        frames += 1
                        tmpColours = colours.copy()
                        tmpColours[left] = focusColour
                        tmpColours[right] = focusColour
                        tmpColours[b] = 'y'
                        self.plotDataUpdated.emit(0, curArray, tmpColours, alphaVals)
                        time.sleep(sleepTime)
                    
                # Swap if we are not in one of the situations described above
                if left < right:
                    curArray[left], curArray[right] = curArray[right], curArray[left]
                    
                    if plot:
                        frames += 1
                        tmpColours = colours.copy()
                        tmpColours[left] = focusColour
                        tmpColours[right] = focusColour
                        tmpColours[b] = 'y'
                        self.plotDataUpdated.emit(0, curArray, tmpColours, alphaVals)
                        time.sleep(sleepTime)
                        
                if left <= right:
                    left += 1
                    right -= 1
                    
                    if plot:
                        frames += 1
                        tmpColours = colours.copy()
                        tmpColours[left] = focusColour
                        tmpColours[right] = focusColour
                        tmpColours[b] = 'y'
                        self.plotDataUpdated.emit(0, curArray, tmpColours, alphaVals)
                        time.sleep(sleepTime)
                    
            # Remember that left either marks the first position of an element >= than our pivot
            # or equals b
            curArray[left], curArray[b] = curArray[b], curArray[left]
            
            if plot:
                frames += 1
                colours[left] = fixedColour
                self.plotDataUpdated.emit(0, curArray, colours, alphaVals)
                time.sleep(sleepTime)
            
            # Push the larger subproblem first on the stack
            if left - a > b - left:
                stack.append((a, left-1))
                if left + 1 < b:
                    stack.append((left + 1, b))
            else:
                if left + 1 < b:
                    stack.append((left + 1, b))
                if a < left - 1:
                    stack.append((a, left-1))
            
        if plot:
            frames += 1
            self.plotDataUpdated.emit(0, curArray, [stdColour] * curNum, [0.5] * curNum)
            
            timeLapsed = time.time() - tstart
            if timeLapsed > 0:
                print('FPS:', frames/timeLapsed)
    
    def heapSort(self, curArray, plot=True):
        '''Apply in-place heap sort to the given array.'''
        
        curNum = len(curArray)
        
        # Start timing
        if plot:
            tstart = time.time()    
            frames = 0
            
            colours = [stdColour] * curNum
        
        # The left part of our array (up to index i) will be a maximum-oriented heap
        
        # First, we start with an empty heap and for i=1,..., n add the element
        # at position i-1
        for i in range(1,curNum):
            # We are adding the element at position i
            parentPos = (i-1) // 2
            curPos = i
            swapped = True
            
            if plot:
                frames += 1
                tempColours = colours.copy()
                tempColours[i] = focusColour
                self.plotDataUpdated.emit(0, curArray, tempColours, [0.5] * curNum)
                time.sleep(sleepTime)

            
            while swapped and (parentPos >= 0):
                # Make sure that the number at position curPos is <= than
                # the number at position parentPos
                if curArray[curPos] > curArray[parentPos]:
                    curArray[curPos], curArray[parentPos] = curArray[parentPos], curArray[curPos]
                    if plot:
                        frames += 1
                        tempColours = colours.copy()
                        tempColours[parentPos] = focusColour
                        self.plotDataUpdated.emit(0, curArray, tempColours, [0.5] * curNum)
                        time.sleep(sleepTime)
                else:
                    swapped = False
                
                curPos = parentPos
                parentPos = (curPos - 1) // 2 
      
        if plot:
            frames += 1
            self.plotDataUpdated.emit(0, curArray, [stdColour] * curNum, [0.5] * curNum)
            time.sleep(sleepTime)

        # Then we will get iteratively the largest element in the heap
        # and swap it with the (n - 1 - i)-th element for i = 0, ..., n-2
        for curLim in range(curNum-1, 0, -1):
            # Swap the i-th element with the (curNum - 1 - i)-th element
            curArray[0], curArray[curLim] = curArray[curLim], curArray[0]
            
            if plot:
                frames += 1
                tempColours = colours.copy()
                tempColours[0] = focusColour
                tempColours[curLim] = fixedColour
                self.plotDataUpdated.emit(0, curArray, tempColours, [0.5] * curNum)
                time.sleep(sleepTime)
                
            curPos = 0
            swapped = True
            # Get the position of the smaller child
            leftChildPos = 2*curPos + 1
            rightChildPos = 2*curPos + 2
            if (rightChildPos < curLim):
                if (curArray[leftChildPos] > curArray[rightChildPos]):
                    childPos = leftChildPos
                else:
                    childPos = rightChildPos
            elif leftChildPos < curLim:
                childPos = leftChildPos
            else:
                swapped = False
            
            while swapped:
                # Make sure that the number at position curPos is >= than
                # the number at position childPos
                if curArray[curPos] < curArray[childPos]:
                    curArray[curPos], curArray[childPos] = curArray[childPos], curArray[curPos]
                    if plot:
                        frames += 1
                        tempColours = colours.copy()
                        tempColours[childPos] = focusColour
                        tempColours[curLim] = fixedColour
                        self.plotDataUpdated.emit(0, curArray, tempColours, [0.5] * curNum)
                        time.sleep(sleepTime)
            
                else:
                    swapped = False
                
                curPos = childPos
                
                # Get the position of the smaller child
                leftChildPos = 2*curPos + 1
                rightChildPos = 2*curPos + 2
                if (rightChildPos < curLim):
                    if (curArray[leftChildPos] > curArray[rightChildPos]):
                        childPos = leftChildPos
                    else:
                        childPos = rightChildPos
                elif leftChildPos < curLim:
                    childPos = leftChildPos
                else:
                    swapped = False
              
        if plot:
            frames += 1
            self.plotDataUpdated.emit(0, curArray, [stdColour] * curNum, [0.5] * curNum)
            
            timeLapsed = time.time() - tstart
            if timeLapsed > 0:
                print('FPS:', frames/timeLapsed)        

    @pyqtSlot()
    def sort(self):
        if self.sortAlgoIndex == 0:
            self.bubbleSort(self.curArray)
        elif self.sortAlgoIndex == 1:
            self.mergeSort(self.curArray)
        elif self.sortAlgoIndex == 2:
            self.quickSort(self.curArray)
        elif self.sortAlgoIndex == 3:
            self.heapSort(self.curArray)
        
        self.finished.emit()