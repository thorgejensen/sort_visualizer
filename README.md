# Sorting Visualizer

Welcome to the Sorting Visualizer! Since I like sorting algorithms and the 
data structures involved, I wrote this little Python tool to illustrate 
them in action. 

It uses the following packages:

*  [PyQt5](https://pypi.org/project/PyQt5/), 
*  [matplotlib](https://matplotlib.org/)

which you can easily install using
```
python -m pip install PyQt5
python -m pip install matplotlib
```

I hope you enjoy playing around with my little tool!